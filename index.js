let express= require('express');

let contestador=require("./contestador.js");

let app=express();

//1. Función normal
//2. Función anónima
//3. Función flecha
//4. como un metodo en la clase

let miObjeto= new contestador();

app.get('/',miObjeto.responder);//pedir datos

app.post('/insertar',miObjeto.insertar);//enviar datos

app.listen(4000);